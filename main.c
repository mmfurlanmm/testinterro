#include <stdio.h>
#include <stdlib.h>
#include <time.h>



int reserveJetons = 10; // Nombre de jetons disponibles. On commence avec 10 jetons
int noirOuRouge=0; // Variable dans laquelle on stocke le choix du joueur
int nbJetonsMise = 1; // Nombre de jetons mis�s
int resultatRoulette; // Nombre obtenu � la roulette
int roulettePaireImpaire; // D�termine si le nombre obtenu � la roulette est pair (rouge) ou impair (noir)
int barillet[6]= {0,0,0,0,0,0}; //On d�clare un tableau vide pour le barillet
int joueurVivant = 1;
int i;



//Fonction ROULETTE
int roulette()
{
    printf("Misez-vous sur NOIR ou ROUGE ? 1=NOIR / 2=ROUGE : ");
    scanf("%d", &noirOuRouge);
    while ((noirOuRouge != 1) && (noirOuRouge != 2))
    {
        printf("Vous devez choisir 1 ou 2 !!\nMisez-vous sur NOIR ou ROUGE ? 1=NOIR / 2=ROUGE : ");
        scanf("%d", &noirOuRouge);
    }


    printf("Combien de jetons misez-vous ? : ");
    scanf("%d", &nbJetonsMise);

    while (nbJetonsMise<1 || nbJetonsMise>25 || nbJetonsMise>reserveJetons)
    {
        if (nbJetonsMise<1) // Mise d'1 jeton minimum
        {
            printf("vous devez miser au moins 1 jeton !!\n");

        }
        else if (nbJetonsMise>25) // Mise de 25 jetons maximum
        {
            printf("vous ne pouvez miser que 25 jetons maximum !!\n");

        }
        else
        {
            printf("vous ne disposez pas d'assez de jetons !!\n");

        }
        printf("Combien de jetons misez-vous ? : ");
        scanf("%d", &nbJetonsMise);
    }



    //On lance la roulette, on g�n�re un nombre al�atoire entre 0 et 36
    for (i=0; i<37; i++)
    {
        resultatRoulette = rand()%37;
    }

    // On v�rifie si le r�sultat est pair ou impair (rouge ou noir) et on stocke la valeur dans la variable roulettePaireImpaire. Si on tombe sur 0, on perd!
    if (resultatRoulette%2 == 0)
    {
        roulettePaireImpaire = 2;
    }
    else if (resultatRoulette == 0)
    {
        roulettePaireImpaire = 0;
    }

    else
    {
        roulettePaireImpaire = 1;
    }

    return roulettePaireImpaire;

}


//Fonction ROULETTE RUSSE
int rouletteRusse()
{
    printf("\nVous n'avez plus de jetons... Vous devez jouer votre vie a la roulette russe !!\n\n");

    barillet[rand()%6]=1; //on place une balle dans le barillet
    int detente = 0; //valeur du tableau � laquelle le coup est tir�
    int ouiNon=1; // variable : souhaitez-vous retenter la roulette russe ?


    do // boucle : souhaitez-vous retenter la roulette russe ?
    {

        if (barillet[detente]!=1)
        {
            printf("Clic ! Ouf, chambre vide! Vous survivez et vous gagnez 20 jetons !\n\n");
            reserveJetons += 20;
            detente ++; //on passe � la chambre suivante du barillet
            printf("Voulez-vous rententer la roulette russe ? 1=OUI / 2=NON : ");
            scanf("%d", &ouiNon);

            while ((ouiNon != 1) && (ouiNon != 2))
            {
                printf("Vous devez choisir 1 ou 2 !! 1=OUI / 2=NON : ");
                scanf("%d", &ouiNon);
            }

        }
        else
        {
            printf("Clic ! BAM !!!!! Une balle dans la tete ! \nVous etes mort...");
            printf("Au moins vous n'aurez plus de probleme avec la mafia, votre famille ne peut pas en dire autant...\n\n");
            joueurVivant = 0;
        }
    }
    while (ouiNon==1 && joueurVivant);

    return reserveJetons;
    return joueurVivant;

}



int main()
{

    srand(time(NULL));

    printf("************* Le jeu de la Mort *************\n\n\n");




    // La partie continue tant que le joueur a encore des jetons et est vivant
    while ((joueurVivant)&&(reserveJetons < 100))
    {

        // On commence par la roulette
        roulette();


        //V�rification si joueur gagne ou perd
        if (roulettePaireImpaire == noirOuRouge)
        {
            if (roulettePaireImpaire == 1)
            {
                printf("%d est sorti, il est noir !\n", resultatRoulette);
            }
            else
            {
                printf("%d est sorti, il est rouge !\n", resultatRoulette);
            }
            printf("Gagne ! \n");
            reserveJetons = reserveJetons + nbJetonsMise; //le joueur double sa mise
        }
        else
        {
            if (roulettePaireImpaire == 1)
            {
                printf("%d est sorti, il est noir !\n", resultatRoulette);
            }
            else if (roulettePaireImpaire == 0)
            {
                printf("%d est sorti !\n", resultatRoulette);
            }
            else
            {
                printf("%d est sorti, il est rouge !\n", resultatRoulette);
            }
            printf("Perdu... \n");
            reserveJetons = reserveJetons - nbJetonsMise; //le joueur perd sa mise
        }
        printf("\nIl vous reste %d jetons.\n\n", reserveJetons);




        //partie de roulette perdue, la partie de ROULETTE RUSSE se lance
        if (reserveJetons == 0)
        {
            rouletteRusse();

        }

        //Partie gagn�e
        if (reserveJetons >= 100)
        {
            printf("Vous avez gagne !! Vous vivez pour cette fois, mais arretez de deconner maintenant !\n\n");

        }


    }





    return 0;
}
