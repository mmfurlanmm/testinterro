#include <stdio.h>
#include <stdlib.h>

int main()
{

    // Exercice 1:
    int i=0;

    printf("Boucle WHILE :\n");

    while (i<10)
    {
        i++;
        printf("%d\n", i);
    }

    printf("\nBoucle FOR :\n");

    for (i=1; i<=10; i++)
    {
        printf("%d\n", i);
    }

    // Exercice 2:
    /* Dans les deux cas, on cr�e un �l�ment auquel on attribue une valeur non modifiable qui peut �tre rappel� dans le programme en utilisant son nom.
    La diff�rence r�side dans le fait que pour la constante, on cr�e effectivement un espace dans la RAM pour stocker cet �l�ment alors que dans le cas
    du #define, il n'y a pas d'espace cr�� dans la RAM, la valeur est seulement attribu�e � chaque fois que le nom est utilis�. */

    // Exercice 3:

    int variablePourExemple;

    printf("\n\nAvec un SWITCH\nEntrez une valeur entre 1 et 3 : ");
    scanf("%d", &variablePourExemple);

    switch (variablePourExemple)
    {
    case 1:
        printf("la valeur est 1");
        break;
    case 2:
        printf("la valeur est 2");
        break;
    case 3:
        printf("la valeur est 3");
        break;
    default:
        printf("on avait dit entre 1 et 3 !!");
        break;
    }

    printf("\n\nAvec une suite de if...else\nEntrez une valeur entre 1 et 3 : ");
    scanf("%d", &variablePourExemple);

    if (variablePourExemple == 1)
    {
        printf("la valeur est 1");
    }
    else if (variablePourExemple == 2)
    {
        printf("la valeur est 2");
    }
    else if (variablePourExemple == 1)
    {
        printf("la valeur est 3");
    }
    else
    {
        printf("on avait dit entre 1 et 3 !!");
    }


    return 0;
}
